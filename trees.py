# -*- coding: utf-8 -*-
"""trees.ipynb

Automatically generated by Colab.

Original file is located at
    https://colab.research.google.com/drive/130tlOZ5ZUyjF2Q7lndbc40yH0OnnMIsT
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv(
  '2015-street-tree-census-tree-data.csv')

df

df1= df.isnull().sum()

df1

fig, ax = plt.subplots()
ax.barh(df1.index, df1)

sns.pairplot(df)

df.dtypes

sns.heatmap(df[['latitude', 'longitude', 'x_sp','y_sp', 'council district', 'census tract', 'bin', 'bbl']].corr(), annot=True)

x = df['latitude']
y = df['longitude']
plt.scatter(x, y)