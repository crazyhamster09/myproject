FROM mambaorg/micromamba

WORKDIR /app

RUN micromamba create -n example python=3.11 jupyter -c conda-forge -y

ENTRYPOINT ["micromamba", "run", "-n", "example", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root" ]
