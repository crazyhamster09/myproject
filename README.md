Методология ведения репозитория, предложенная авторами курса MlOps 3.0.

Репозиторий содержит Dockerfile.

Сборка:

`docker build . -t mlops_app:latest ./`

Запуск:

`docker run -it --name mlops_container --rm mlops_app:latest`
